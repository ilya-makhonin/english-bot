# -*- coding: utf-8 -*-
import logging
import multiprocessing
from telebot import apihelper
import telebot

import log
from constants import *
from sql import *
from utils import *
import yandex_api
from config import Config
import time
from states.errors import *
from states.state import States
from chatbase_statistics import Statistics

bot = telebot.TeleBot(Config.token)
states = States(Config.ip, Config.db_port, Config.db_name, Config.db_login, Config.db_pass)
user_statistics = Statistics(Config.chatbase_token)  # Управление статистикой бота
bot_logger = log.logger('bot', 'logs.log')

main_mark = create_markup(main_mark_buttons)
coach_mark = create_markup(coach_fields[:-1], 2)
back_mark = create_markup([coach_fields[4]])


@bot.message_handler(commands=['start'])
def start_handler(message):
    user_id = message.from_user.id
    is_new = add_user(user_id, message.from_user.username)
    if is_new is None:
        user_statistics.send_statistics(user_id, "New User", "/start")
        bot.send_message(user_id, beginner_message,
                         reply_markup=create_markup([start_training, knowledge_test, set_my_rank]))
    else:
        user_statistics.send_statistics(user_id, "Unique Message", "/start")
        bot.send_message(user_id, start_mes, reply_markup=main_mark)


@bot.message_handler(commands=['help'])
def help_handler(message):
    bot.send_message(message.from_user.id, help_message, parse_mode="Markdown")


@bot.message_handler(regexp="Переводчик")
def translator(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Переводчик")
    states.change_state(message, "translate_the_word")
    bot.send_message(user_id, send_me_word, reply_markup=back_mark)


@bot.message_handler(func=lambda message: message.chat.id == feedback_chat_id and message.reply_to_message is not None)
def channel_handler(message):
    # user_id = message.reply_to_message.forward_from.id
    user_id = message.reply_to_message.text.split('\n')[0]
    bot.send_message(user_id, message.text)


@bot.message_handler(func=lambda message: states.handle(message, "translate_the_word"))
def translate_the_word(message):
    user_id = message.from_user.id
    if message.text in [*coach_fields, '/start']:
        states.change_state(message, "menu")
        bot.send_message(user_id, start_mes, reply_markup=main_mark)
    else:
        bot.send_message(user_id, yandex_api.translate_word(message.text))


@bot.message_handler(commands=['feedback'])
@bot.message_handler(regexp="Обратная связь")
def feedback_handler(message):
    user_id = message.from_user.id
    states.change_state(message, "send_feedback")
    bot.send_message(user_id, feedback_message, reply_markup=create_markup(["Отмена"]))


@bot.message_handler(func=lambda message: states.handle(message, "send_feedback") and message.content_type is 'text')
def send_feedback(message):
    user_id = message.from_user.id
    states.change_state(message, "menu")
    if message.text not in [*coach_fields, *main_mark_buttons, "Отмена", "Тренер", "Переводчик"]:
        # bot.forward_message(feedback_chat_id, user_id, message.message_id)
        bot.send_message(feedback_chat_id, f"{user_id}\n@{message.from_user.username}\n{message.text}")
        bot.send_message(user_id, "Мы получили ваше сообщение!")
    bot.send_message(user_id, start_mes, reply_markup=main_mark)


@bot.message_handler(regexp="Тренер")
@bot.callback_query_handler(func=lambda callback: callback.data == test_exit_message)
def coach_handler(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Тренер")
    is_test = states.handle(message, "test_")
    if states.handle(message, "exercise") or is_test:
        adv_text = view_adv()
        if adv_text:   # ****************** Advertising is here ******************
            bot.send_message(user_id, adv_text, parse_mode="HTML")
            user_statistics.send_statistics(user_id, "Advertising", "Тренер")
    if is_test:
        bot.edit_message_reply_markup(user_id, message.message.message_id)
    states.change_state(message, "menu")
    bot.send_message(user_id, coach_message, reply_markup=coach_mark)


@bot.callback_query_handler(func=lambda callback: callback.data == "main_menu")
@bot.message_handler(regexp="Главное меню")
def back_to_main_menu(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Главное меню")
    bot.send_message(user_id, start_mes, reply_markup=main_mark)


@bot.message_handler(regexp="Мой прогресс")
def show_progress(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Мой прогресс")
    user_progress_list = get_progress_categories(user_id, 0, 101)
    user_progress_test = get_progress_test(user_id)
    user_rank = get_user_rank(user_id)

    if not user_progress_list and not user_progress_test:
        text = finished_not_found
        markup = create_markup([coach_fields[2], coach_fields[-1]], 2)
    else:
        text = f"\U0001F393 Ваш уровень: *{ranks[int(user_rank) - 1]}*\n\n"
        text += "Ваш прогресс:\n\n" if user_progress_list else ''
        for category in user_progress_list:
            emoji = "\U00002705" if category[2] == 100 else "\U00002611"
            text += f"{emoji} *{category[1]}  {category[2]}%*\n"
        text += "\nЗавершенные тесты:\n\n" if user_progress_test else ''
        for test in user_progress_test:
            splited_result = test[1].split("*")
            score = int(splited_result[-2]) / int(splited_result[1])
            if score >= 3:
                emoji = "\U0001F625"
            elif 3 > score > 1.5:
                emoji = "\U0001F60F"
            else:
                emoji = "\U0001F913"
            text += f"{emoji} *{test[0]}*  {test[1]}\n"
        markup = create_markup([coach_fields[-1]])
    bot.send_message(user_id, text, reply_markup=markup, parse_mode="Markdown")


@bot.message_handler(regexp="Повторить изученное")
def repeat_studied(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Повторить изученное")
    user_progress_list = get_progress_categories(user_id, 100, 101)  # только выученные наборы
    text = nothing_found
    markup = create_buttons_list(user_progress_list)
    if user_progress_list:
        text = unfinished_message
        states.change_state(message, "choose_repeat_category")
    bot.send_message(user_id, text, reply_markup=markup)


@bot.message_handler(func=lambda message: states.handle(message, "choose_repeat_category"))
def choose_repeat_category(message):
    user_id = message.from_user.id
    for category in get_progress_categories(user_id, 100, 101):
        if category[1] in message.text:
            words = get_words_for_repeat(category[0])
            states.change_state(message, f"repetition|{category[0]}|1")
            text = create_word_text(words[0])
            markup = create_markup(["\U000023E9 Далее", coach_fields[-1]], 2)
            sound = get_sound(words[0][0])
            if sound:
                bot.send_voice(user_id, sound, text, reply_markup=markup, parse_mode='Markdown')
            else:
                bot.send_message(user_id, text, reply_markup=markup, parse_mode='Markdown')


@bot.message_handler(func=lambda message: states.handle(message, "repetition"))
def repetition(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Повторение")
    state = states.db.get_state(user_id).split("|")
    word_index = int(state[2])
    words = get_words_for_repeat(state[1])
    if word_index == len(words):
        states.change_state(message, "menu")
        end_markup = create_markup([coach_fields[-1], coach_fields[3]], 2)
        bot.send_message(user_id, set_repeated, reply_markup=end_markup)
    else:
        states.change_state(message, f"repetition|{state[1]}|{word_index + 1}")
        text = create_word_text(words[word_index])
        markup = create_markup(["\U000023E9 Далее", coach_fields[-1]], 2)
        sound = get_sound(words[word_index][0])
        if sound:
            bot.send_voice(user_id, sound, text, reply_markup=markup, parse_mode='Markdown')
        else:
            bot.send_message(user_id, text, reply_markup=markup, parse_mode='Markdown')


@bot.message_handler(regexp="Незавершенное")
def show_unfinished(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Незавершенное")
    user_progress_list = get_progress_categories(user_id, 0, 100)  # только незаконченные наборы
    markup = create_buttons_list(user_progress_list)
    text = nothing_found
    if user_progress_list:
        text = unfinished_message
        states.change_state(message, "category_choosing")
    bot.send_message(user_id, text, reply_markup=markup)


@bot.message_handler(regexp="Начать заниматься")
@bot.message_handler(regexp="Каталог")
def catalog_handler(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Каталог")
    states.change_state(message, "section_choosing")
    text = catalog_message
    markup = create_buttons_list(get_sections())
    bot.send_message(user_id, text, reply_markup=markup)


@bot.message_handler(regexp=set_my_rank)
def choosing_user_rank_handler(message):
    user_id = message.from_user.id
    button = telebot.types.InlineKeyboardButton
    markup = telebot.types.InlineKeyboardMarkup()
    markup.row(button("Beginner", callback_data="1"))
    markup.add(*[button(ranks[i - 1], callback_data=str(i)) for i in range(2, 8)])
    markup.row(button('Главное меню', callback_data="main_menu"))
    states.change_state(message, "setting_rank")
    bot.send_message(user_id, set_rank_text, reply_markup=markup)


@bot.callback_query_handler(func=lambda x: states.handle(x, "setting_rank"))
def set_user_rank_handler(callback):
    user_id = callback.from_user.id
    bot.edit_message_text(callback.message.text + f"\n\U00002705 Ваш уровень: {ranks[int(callback.data) - 1]}",
                          user_id, callback.message.message_id)
    update_user_rank(user_id, callback.data)
    states.change_state(callback, "menu")
    bot.send_message(user_id, start_mes, reply_markup=main_mark)


@bot.message_handler(regexp=knowledge_test)
def knowledge_test_handler(message):
    states.start_test(message, bot, 2)  # По умолчанию id теста на знание английского == 2


@bot.message_handler(regexp="Тесты")
def test_handler(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Тесты")
    states.change_state(message, "choosing_test")
    text = "Выберите тест для прохождения:"
    markup = create_buttons_list(get_test_names())
    bot.send_message(user_id, text, reply_markup=markup)


# **************************************** Admin panel function ****************************************

@bot.message_handler(commands=['globalmailing'])  # It's global mailing function. For system alerts, not advertising
def global_mailing_choosing(message: telebot.types.Message):
    admins = [admin[0] for admin in get_admins_id()]  # admins list like [id, id, ...]
    if message.from_user.id in admins:
        states.change_state(message, "global_mailing_method")
        bot.send_message(message.from_user.id, "Отправь мне текстовое сообщение или фото")


@bot.message_handler(content_types=['text', 'photo'], func=lambda m: states.handle(m, "global_mailing_method"))
def global_mailing_photo(message: telebot.types.Message):
    states.change_state(message, "menu")
    counter_deleted_users = 0
    users_list = get_users()  # users list like [(id,), (id,), ...]
    for user in users_list:
        try:
            if message.content_type == 'text':
                bot.send_message(user[0], message.text, parse_mode='HTML')  # send message to all users
            elif message.content_type == 'photo':
                bot.send_photo(user[0], message.photo[2].file_id, message.caption, parse_mode="HTML")
        except Exception as error:
            counter_deleted_users += 1
            bot_logger.warning(error)
            continue
    bot.send_message(
        message.from_user.id,
        'На данный момент количество удалённых пользователей равно {}'.format(counter_deleted_users))


@bot.message_handler(commands=['poll'])
def global_poll_mailing(message):
    admins = [admin[0] for admin in get_admins_id()]  # admins list like [id, id, ...]
    if message.from_user.id in admins:
        states.change_state(message, "text_poll")
        bot.send_message(message.from_user.id, "Отправь мне текст для опроса")


@bot.message_handler(func=lambda m: states.handle(m, "text_poll"))
def poll_text_handler(message):
    add_poll_text(message.text)
    states.change_state(message, "answers_poll")
    bot.send_message(message.from_user.id, "А теперь список ответов в формате answer|answer|answer...")


@bot.message_handler(func=lambda m: states.handle(m, "answers_poll"))
def poll_answers_handler(message):
    add_poll_answers(message.text.split('|'))
    states.change_state(message, "confirm_poll")
    poll = get_latest_poll()
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton("Отправить", callback_data="confirm_poll"),
               types.InlineKeyboardButton("Удалить", callback_data="delete_poll"))
    bot.send_message(message.from_user.id, "Текст: {}\n\nОтветы:\n{}\n\nЕсли все верно, нажмите кнопку "
                                           "<Отправить>".format(poll[1], '\n'.join(poll[2])), reply_markup=markup)


@bot.callback_query_handler(func=lambda m: states.handle(m, "confirm_poll"))
def confirm_poll_handler(callback):
    states.change_state(callback, "menu")
    poll = get_latest_poll()
    if callback.data == 'confirm_poll':
        counter_deleted_users = 0
        users_list = get_users()  # users list like [(id,), (id,), ...]
        for user in users_list:
            try:
                markup = types.InlineKeyboardMarkup(row_width=1)
                markup.add(*[types.InlineKeyboardButton(answer, callback_data=f'vote_{ind}_{poll[0]}')
                             for ind, answer in enumerate(poll[2])])
                bot.send_message(user[0], poll[1], parse_mode='HTML', reply_markup=markup)
            except Exception as error:
                counter_deleted_users += 1
                bot_logger.warning(error)
                continue
        bot.edit_message_text(f'На данный момент количество удалённых пользователей равно {counter_deleted_users}',
                              callback.from_user.id, callback.message.message_id)
    elif callback.data == "delete_poll":
        delete_poll(poll[0])
        bot.edit_message_text(callback.message.text + "\n\n\U0000274C Удалено",
                              callback.from_user.id, callback.message.message_id)


@bot.message_handler(commands=['latestpoll'])
def latest_poll(message):
    admins = [admin[0] for admin in get_admins_id()]  # admins list like [id, id, ...]
    if message.from_user.id in admins:
        poll = get_latest_poll()
        bot.send_message(message.from_user.id, "{}\n\n{}".format(poll[1], '\n'.join(
            f"{answer} - <b>{poll[3][ind]} голосов</b>" for ind, answer in enumerate(poll[2]))), parse_mode="HTML")


@bot.message_handler(commands=['advertising'])  # It's send advertising function
def add_advertising(message: telebot.types.Message):
    admins = [admin[0] for admin in get_admins_id()]
    if message.from_user.id in admins:
        text = message.text[13:].strip(' ')
        if text == '':
            bot.send_message(
                message.from_user.id,
                'Введите рекламный текст! /advertising длительность|ваш текст '
                '(можете не вводить продолжительность, по умолчанию это неделя)')
        else:
            text = text.split('|')
            if len(text) == 1:
                result = send_adv(text[0].strip())  # Send new advertising text. Data set automatic
            else:
                result = send_adv(text[1].strip(), data=text[0].strip())
            text = 'Создан рекламный пост: \nТекст: {} \nДата старта рассылки: {} \nДата окончания рассылки: {}' \
                .format(result['text'], result['lower'], result['upper'])
            bot.send_message(message.from_user.id, text, parse_mode='HTML')


@bot.message_handler(commands=['getstatistics'])
def get_stat(message: telebot.types.Message):
    admins = [admin[0] for admin in get_admins_id()]
    if message.from_user.id in admins:
        count_users = get_count_users()
        text = 'Всего пользователей: {}'.format(count_users)
        bot.send_message(message.from_user.id, text)


@bot.message_handler(commands=['helping'])
def helper_handle(message: telebot.types.Message):
    admins = [admin[0] for admin in get_admins_id()]
    if message.from_user.id in admins:
        text = '/globalmailing - глобальная немедленная рассылка\n(<b>/globalmailing</b> <i>текст_рассылки</i>)\n' \
               '/advertising - загрузка рекламы для рассылки\n(<b>/advertising</b> <i>длительность</i>|' \
               '<i>рекламный_текст</i> - можете не вводить продолжительность, по умолчанию это неделя)\n' \
               '/getstatistics - статистика бота\n/fulladv - список рекламы\n' \
               '/editword - корректировать слова в базе данных (<b>*beta*</b>)'
        bot.send_message(message.from_user.id, text, parse_mode='HTML')


@bot.message_handler(commands=['fulladv'])
def full_advertising(message: telebot.types.Message):
    admins = [admin[0] for admin in get_admins_id()]
    if message.from_user.id in admins:
        list_adv = get_full_adv()
        if len(list_adv) == 0:
            text = 'На данный момент реклама отсутствует. ' \
                   'Для создания рекламной рассылки используйте команду /advertising'
        else:
            text = ''
            for adv in list_adv:
                date = '{} - {}'.format(adv[2].lower, adv[2].upper)
                text += 'Текст: {}\nКол-во просмотров: {}\nДата: {}\n\n'.format(adv[0], adv[1], date)
        bot.send_message(message.from_user.id, text, parse_mode='HTML')


@bot.message_handler(commands=['editword'])
def edit_word_handler(message: telebot.types.Message):
    """
    Функция для правки слов через бота (Кастро, почекай её, так как мне больше нравиться идея работы со словами через
    pgAdmin или через подобные приложения
    """
    admins = [admin[0] for admin in get_admins_id()]
    if message.from_user.id in admins:
        bot.send_message(message.from_user.id,
                         'Введите в сообщении язык слова, по которому будет производиться поиск в базе (ru/en)\n'
                         'Затем введите слово, в котором содержиться ошибка и верный вариант этого слова '
                         '(выбранный язык должен совпадать с языком слова с ошибкой) Затем введите слово по которому'
                         ' будет производиться поиск для проверки на другом языке. Регистр не имеет значения!\n\n'
                         '*Все составляющие разделяйте вертикальной чертой - |*\n\n'
                         'Пример: ru|спуна|спина|back или en|blick|black|чёрный',
                         parse_mode='markdown', reply_markup=create_markup(['Back']))
        bot.register_next_step_handler_by_chat_id(message.from_user.id, edit_word_callback)


def edit_word_callback(message: telebot.types.Message):
    if message.text == 'Back':
        bot.send_message(message.from_user.id, start_mes, reply_markup=main_mark)
    else:
        parce = message.text.split('|')
        if len(parce) < 4:
            bot.send_message(message.from_user.id,
                             'Введены некорректные данные, либо они нарушают форму записи. Попробуёте ещё!\n\n'
                             '*Пример: ru|спуна|спина|back или en|blick|black|чёрный*', parse_mode='markdown')
            bot.register_next_step_handler_by_chat_id(message.from_user.id, edit_word_callback)
        else:
            result = edit_word(parce[0].strip().lower(), parce[1].strip().lower(),
                               parce[2].strip().lower(), parce[3].strip().lower())
            if len(result) == 0:
                text = 'В базе не было найдено подобных слов!'
            else:
                text = ''
                for i in result:
                    text += '{}\nID - {}\nEN - {}\nRU - {}\n\n'.format(i[4], i[0], i[1], i[2])
            bot.send_message(message.from_user.id, start_mes, reply_markup=main_mark)


# ******************************************************************************************************


@bot.callback_query_handler(func=lambda callback: "vote" in callback.data)
def vote_handler(callback):
    vote_id, poll_id = callback.data.split('_')[1:]
    text, answer = new_vote(int(vote_id), int(poll_id))
    bot.edit_message_text(text + f"\n\n\U00002705 {answer}",
                          callback.from_user.id, callback.message.message_id, parse_mode='HTML')


@bot.message_handler(func=lambda message: states.handle(message, "section_choosing"))
def section_handler(message):
    user_id = message.from_user.id
    for section in get_sections():
        if section[1] in message.text:
            states.change_state(message, "category_choosing")
            markup = create_buttons_list(get_categories(section[0]))
            text = "Выберите набор:"
            bot.send_message(user_id, text, reply_markup=markup)


@bot.message_handler(func=lambda message: states.handle(message, "category_choosing"))
def category_handler(message):
    user_id = message.from_user.id
    for category in get_categories():
        if category[1] in message.text:
            user_statistics.send_statistics(user_id, "Famous Category", category[1])
            text, markup, correct_answer, word_id, progress, sound = create_lesson_step(user_id, category[0])
            states.change_state(message, f"exercise|{word_id}|{correct_answer}|{category[0]}|{progress}")
            if sound:
                bot.send_voice(user_id, sound, text, reply_markup=markup, parse_mode="Markdown")
            else:
                bot.send_message(user_id, text, reply_markup=markup, parse_mode='Markdown')


@bot.message_handler(func=lambda message: states.handle(message, "exercise"))
def exercise_handler(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Упражнение")
    user_answer = message.text
    _, word_id, correct_answer, category_id, progress = states.db.get_state(user_id).split("|")
    word_progress = float(progress)
    text = incorrect_message.format(correct_answer)
    if word_progress == 0:
        try:
            insert_word_progress(user_id, word_id, answers[user_answer])
        except KeyError:
            return
    elif user_answer.lower() == correct_answer.lower():
        update_word_progress(user_id, word_id, round(word_progress + 0.2, 1))
        text = correct_message
    elif user_answer == 'Нет, я не помню':
        delete_word_progress(user_id, word_id)
    if word_progress in [0.2, 0.4, 0.6]:
        bot.send_message(user_id, text, parse_mode='Markdown')
    text, markup, correct_answer, word_id, progress, sound = create_lesson_step(user_id, category_id)
    if correct_answer:
        states.change_state(message, f"exercise|{word_id}|{correct_answer}|{category_id}|{progress}")
    if sound:
        bot.send_voice(user_id, sound, text, reply_markup=markup, parse_mode="Markdown")
    else:
        bot.send_message(user_id, text, reply_markup=markup, parse_mode='Markdown')


@bot.message_handler(func=lambda message: states.handle(message, "choosing_test"))
def test_start(message):
    for test in get_test_names():
        if test[1] in message.text:
            # last arg is test id (may use simple number (1 in that case))
            states.start_test(message, bot, test[0])


@bot.callback_query_handler(func=lambda x: states.handle(x, 'test_'))
def test_test(message):
    user_id = message.from_user.id
    user_statistics.send_statistics(user_id, "Unique Message", "Тесты")
    try:
        output = states.handle_test(message, bot, states.db.get_state(user_id).split("_")[1:])
        if output is not None:
            result, progress, test_id = output
            if test_id == 2:
                if 0 <= progress <= 8:
                    rank = 1
                elif 9 <= progress <= 14:
                    rank = 2
                elif 15 <= progress <= 22:
                    rank = 3
                elif 23 <= progress <= 30:
                    rank = 4
                elif 31 <= progress <= 35:
                    rank = 5
                elif 36 <= progress <= 40:
                    rank = 6
                result += f"\nВаш уровень: {ranks[rank]}"
                update_user_rank(user_id, rank + 1)

            states.change_state(message, 'menu')
            bot.send_message(user_id, f'Тест завершен!\nРезультат: {result}',
                             parse_mode="Markdown", reply_markup=create_markup(coach_fields[-2:], 2))
    except UserNotFoundException:
        states.change_state(message, 'menu')


def args_check(args_names, checking_kwargs):
    """
    :param args_names: <list> names of variables
    :param checking_kwargs: <map/kwargs> map with variables names and variables by itself
    :return: True if all vars in kwargs, False else
    """
    for arg in args_names:
        if checking_kwargs.get(arg) is None:
            return False
    return True


def bot_start(use_webhook=False, webhook_data={}):
    """
    runs bot without webhook or with it by specified params
    :param use_webhook: <bool> True if you want to run with webhook, False else
    :param webhook_data <map>: <str> webhook_ip - ip to request telegram api
                   <str> webhook_port
                   <str> Telebot token
                   <str> ssl_cert - filepath to .pem public ssl key
    """

    def set_webhook(url, cert):
        try:
            apihelper.set_webhook(Config.token, url=url, certificate=cert)
        except Exception as err:
            print(err.with_traceback(None))

    def webhook_isolated_run(url, cert):
        multiprocessing.Process(target=set_webhook, args=(url, cert), daemon=True).start()

    global bot, states

    telebot.logger.setLevel(logging.DEBUG)
    telebot.logger.addHandler(log.__file_handler('logs.log', log.__get_formater()))

    if not use_webhook:
        bot.remove_webhook()
        bot.polling(none_stop=True)

    elif args_check(['webhook_ip', 'webhook_port', 'token', 'ssl_cert'], webhook_data):
        bot.remove_webhook()
        time.sleep(1)

        webhook_isolated_run(url='https://%s:%s/%s/' % (webhook_data.get('webhook_ip'),
                                                        webhook_data.get('webhook_port'),
                                                        webhook_data.get('token')),
                             cert=open(webhook_data.get('ssl_cert'), 'r'))

        return bot
    else:
        raise Exception('Params for start with webhook is not specified')

    return bot
