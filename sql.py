import os
import time
import io
import psycopg2
from psycopg2.extras import DictCursor
import datetime
from config import Config


def get_connection(dict_return=False) -> psycopg2._psycopg.connection:
    """
    :param dict_return: <bool> does cursor should return dictionary or not
    :return: <psycopg2.Connection> connection to db with specified params
    """
    if not dict_return:
        return psycopg2.connect(host=Config.ip, dbname=Config.db_name, user=Config.db_login,
                                password=Config.db_pass, port=Config.db_port)
    else:
        return psycopg2.connect(host=Config.ip, dbname=Config.db_name, user=Config.db_login,
                                password=Config.db_pass, port=Config.db_port, cursor_factory=DictCursor)


def add_user(user_id, username):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT * FROM users WHERE user_id=%s;', (user_id,))
            unique_id = cur.fetchone()  # Проверяем, есть ли юзер в базе. Если нет возвращается None
            cur.execute("INSERT INTO users (user_id, name, rank, state) VALUES (%s, %s, 1, 'beginner') "
                        "ON CONFLICT (user_id) DO UPDATE SET state = 'menu';", (user_id, username))
            conn.commit()
            return unique_id


def get_sections():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM sections;")
            sections = cur.fetchall()
            conn.commit()
            return sections


def get_categories(section_id=None):
    with get_connection() as conn:
        with conn.cursor() as cur:
            if section_id:
                cur.execute("SELECT id, category FROM category WHERE section = %s;", (section_id,))
            else:
                cur.execute("SELECT id, category FROM category;")
            categories = cur.fetchall()
            conn.commit()
            return categories


def get_words(user_id, category_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT words.*, CASE WHEN user_progress.progress IS NOT NULL THEN user_progress.progress "
                        "ELSE 0 END AS progress FROM words LEFT JOIN (SELECT * FROM progress_word JOIN "
                        "users ON users.id = progress_word.user AND users.user_id = %s) AS user_progress ON words.id = "
                        "user_progress.word WHERE words.category = %s;", (user_id, category_id))
            words = cur.fetchall()
            conn.commit()
            return words


def get_words_for_repeat(category_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM words WHERE category = %s;", (category_id,))
            words = cur.fetchall()
            conn.commit()
            return words


def update_word_progress(user_id, word_id, progress):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                "UPDATE progress_word SET progress = %s WHERE \"user\" = (SELECT id FROM users WHERE user_id = %s)"
                " AND word = %s;", (progress, user_id, word_id))
            conn.commit()


def insert_word_progress(user_id, word_id, progress):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO progress_word (\"user\", word, progress) "
                        "VALUES ((SELECT id FROM users WHERE user_id = %s), %s, %s)", (user_id, word_id, progress))
            conn.commit()


def delete_word_progress(user_id, word_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("DELETE FROM progress_word WHERE \"user\" = (SELECT id FROM users WHERE user_id = %s) "
                        "AND word = %s;", (user_id, word_id))
            conn.commit()


def get_progress_categories(user_id, lower_limit, upper_limit):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT category.id, category.category, CASE WHEN progress_word.progress = 2 THEN 1 ELSE "
                        "progress_word.progress END FROM progress_word JOIN words ON progress_word.word = words.id "
                        "JOIN users ON progress_word.user = users.id JOIN category ON words.category = category.id "
                        "WHERE users.user_id = %s;", (user_id,))
            progress_word_list = cur.fetchall()
            progress_dict = dict()
            for point in progress_word_list:
                progress_dict.setdefault(point[:2], 0)
                progress_dict[point[:2]] += point[2]
            progress_categories = list()
            for key, val in progress_dict.items():
                cur.execute("SELECT count(id) FROM words WHERE category = %s;", (key[0],))
                set_len = cur.fetchone()[0]
                percent = min(round((val / set_len) * 100), 100)
                if lower_limit <= percent < upper_limit:
                    progress_categories.append(key + (percent,))
            conn.commit()
            return progress_categories


def get_admins_id():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT user_id FROM admins;")
            admins = cur.fetchall()
            conn.commit()
            return admins


def get_users():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT user_id FROM users;")
            users = cur.fetchall()
            conn.commit()
            return users


def send_adv(text, data='6'):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT * FROM advertising ORDER BY id DESC;")
            advertising_real = cur.fetchone()
            if advertising_real is None:  # Если в таблице ещё не содержиться рекламы
                today = datetime.date.today()
                week_up = today + datetime.timedelta(days=int(data))
                date_public = '[{}, {})'.format(today, week_up)
            else:
                exit_day = advertising_real[3].upper  # Взать верхную дату предыдущей рекламы
                if (datetime.date.today() - exit_day) > datetime.timedelta(days=1):
                    today = datetime.date.today()
                    week_up = today + datetime.timedelta(days=int(data))
                    date_public = '[{}, {})'.format(today, week_up)
                else:
                    week_up = exit_day + datetime.timedelta(days=int(data))
                    date_public = '({}, {}]'.format(exit_day, week_up)
            cur.execute("INSERT INTO advertising (text_adv, date_public) VALUES (%s, %s);", (text, date_public))
            cur.execute("SELECT * FROM advertising ORDER BY id DESC;")
            success = cur.fetchone()
            conn.commit()
            return {
                'text': success[1],
                'lower': success[3].lower,
                'upper': success[3].upper
            }


def view_adv():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT id, text_adv, views, date_public FROM advertising "
                        "WHERE CURRENT_DATE >= lower(date_public) AND CURRENT_DATE <= upper(date_public);")
            real_adv = cur.fetchone()
            if real_adv is None:
                return None
            up_view = real_adv[2] + 1
            cur.execute("UPDATE public.advertising SET views=%s WHERE id=%s;", (up_view, real_adv[0]))
            conn.commit()
            return real_adv[1]


def get_count_users():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT count FROM public.users_count;')
            count = cur.fetchone()
            return count[0]


def get_full_adv():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT text_adv, views, date_public FROM advertising ORDER BY id;')
            full_list = cur.fetchall()
            return full_list


def edit_word(lang, word_error, correct_word, word_search):
    """
    :param lang: Слово, по которому будет производиться поиск (русское или английское)
    :param word_error: Слово для правки и поиска
    :param correct_word: Исправленное слово
    :param word_search: Слово для поиска
    :return: Результат обновления
    """
    with get_connection() as conn:
        with conn.cursor() as cur:
            if lang == 'ru':
                cur.execute('UPDATE words SET word_ru = %s WHERE word_ru = %s;', (correct_word, word_error))
            elif lang == 'en':
                cur.execute('UPDATE words SET word_en = %s WHERE word_en = %s;', (correct_word, word_error))
            conn.commit()
            cur.execute(
                'SELECT words.id, words.word_en, words.word_ru, category.category FROM words '
                'JOIN category ON words.category = category.id WHERE word_en = %s OR word_ru = %s;',
                (word_search, word_search))
            return cur.fetchall()


def get_test_names():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT id, name FROM tests;')
            names = cur.fetchall()
            conn.commit()
            return names


def update_test_progress(user_id, test_id, result, progress):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT progress_tests.result FROM progress_tests JOIN users ON users.id = progress_tests.user '
                        'WHERE users.user_id = %s AND progress_tests.test = %s;', (user_id, test_id))
            prev_result = cur.fetchone()
            if not prev_result:
                cur.execute('INSERT INTO progress_tests (\"user\", test, result) '
                            'VALUES ((SELECT id FROM users WHERE user_id = %s), %s, %s);', (user_id, test_id, result))
                conn.commit()
                return ''
            else:
                if int(prev_result[0].split('*')[1]) < progress:
                    cur.execute('UPDATE progress_tests SET result = %s WHERE \"user\" = '
                                '(SELECT id FROM users WHERE user_id = %s) AND test = %s;', (result, user_id, test_id))
                conn.commit()
                return '\nПредыдущий результат: ' + prev_result[0]


def get_progress_test(user_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT tests.name, progress_tests.result FROM progress_tests '
                        'JOIN tests ON tests.id = progress_tests.test JOIN users ON users.id = progress_tests.user '
                        'WHERE users.user_id = %s;', (user_id,))
            res = cur.fetchall()
            conn.commit()
            return res


def update_user_rank(user_id, rank):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('UPDATE users SET rank = %s WHERE user_id = %s;', (rank, user_id))
            conn.commit()


def get_user_rank(user_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT rank FROM users WHERE user_id = %s', (user_id,))
            rank = cur.fetchone()
            conn.commit()
            return rank[0]


def get_sound(word_id):
    """
    :param word_id: <int> word's id in db
    :return: <byte> binary data of mp3 file
    """
    with get_connection() as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT sound FROM words_sound WHERE id=%s", (word_id,))
            return io.BytesIO(cursor.fetchone()[0])


def add_poll_text(text):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO polls (text) VALUES (%s)", (text,))
            conn.commit()


def add_poll_answers(answers):  # and votes like [0, 0, 0...]
    with get_connection() as conn:
        with conn.cursor() as cur:
            votes = [0] * len(answers)
            cur.execute("UPDATE polls SET answers = %s, votes = %s WHERE id = (SELECT max(id) FROM polls);",
                        (answers, votes))
            conn.commit()


def get_latest_poll():
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT id, text, answers, votes FROM polls WHERE id = (SELECT max(id) FROM polls);")
            poll = cur.fetchone()
            conn.commit()
            return poll


def delete_poll(poll_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("DELETE FROM polls WHERE id = %s", (poll_id,))
            conn.commit()


def new_vote(vote_id, poll_id):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute("SELECT text, answers, votes FROM polls WHERE id = %s;", (poll_id,))
            text, answers, votes = cur.fetchone()
            votes[vote_id] += 1
            cur.execute("UPDATE polls SET votes = %s WHERE id = %s;", (votes, poll_id))
            conn.commit()
            return text, answers[vote_id]
