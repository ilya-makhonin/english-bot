from telebot import types
from constants import *
from sql import *
from random import shuffle, choice


def create_markup(buttons, row_width=1):
    """
    Создает клавиатуру с кнопками из списка buttons и шириной строк row
    :param buttons: <list> like [['button_text', 'callback_data'], ...]
    :param row_width: <int> number of buttons per line
    :return: <class 'telebot.types.InlineKeyboardMarkup'>
    """
    markup = types.ReplyKeyboardMarkup(True, False, row_width=row_width)
    markup.add(*buttons)
    return markup


def create_buttons_list(arr):
    if arr:
        buttons = list()
        for unit in arr:
            percent = '' if len(unit) < 3 else f"  {unit[2]}%"
            button_text = unit[1] + percent
            buttons.append(button_text)

        buttons.append(coach_fields[-1])
        return create_markup(buttons)
    else:
        return create_markup([coach_fields[2], coach_fields[-1]], 2)


def create_word_text(word):
    word_id, en_word, ru_word, transcription, synonyms, _, rank = word
    if transcription is None:
        transcription = ''
    else:
        transcription = "[ " + transcription + " ]"
    if synonyms is None:
        synonyms = ''
    text = f"\U0001F1EC\U0001F1E7 *{en_word.upper()}* {transcription}\n" \
        f"\U0001F1F7\U0001F1FA *{ru_word.upper()}*\n_{', '.join(synonyms)}_"
    return text


def create_lesson_step(user_id, category_id):
    """
    Все это создает упражнение (его текст, клавиатуру, правильный ответ)
    и возвращает id слова, прогресс которого нужно будет обновить + progress (чтобы понять насколько изменить прогресс)
    :param user_id: <int> user id
    :param category_id: <int> id of current category
    :return: text and markup - parameters for sending a message
             correct_answer - for text handler and check user answer (False - if set of words is finished)
             word_id - id of current random word
             progress - progress of current random word
    """
    raw_word_list = get_words(user_id, category_id)  # Необработанный список прогресса по словам
    words_list = list()
    for w in raw_word_list:
        if w[-1] < 1:  # избавляемся от выученных слов
            words_list.append(w)

    sound = None

    if len(words_list) is 0:  # если все выучено
        buttons = [coach_fields[2]]
        text = finish_of_set
        correct_answer = False
        word_id = None
        progress = None
    else:
        correct_answer = True
        buttons = list()
        text = ""

        # случайное слово, разделенное на составляющие
        choice_word = choice(words_list)
        word_id, en_word, ru_word, transcription, synonyms, _, rank, progress = choice_word

        # далее создание кнопок для каждой стадии прогресса
        if progress == 0:
            text += create_word_text(choice_word[:-1])
            buttons = ['\U000023E9 Далее', 'Я уже знаю это слово', 'Я не хочу учить это слово']
            sound = get_sound(word_id)
        elif progress == 0.2:
            text += f"\U0001F1F7\U0001F1FA *{ru_word.upper()}*\nВыберите правильный ответ"
            buttons = [en_word]
            i = 0
            while i < 3 and len(buttons) < 4:
                word = choice(raw_word_list)[1]
                if word not in buttons:
                    buttons.append(word)
                    i += 1
            shuffle(buttons)
            correct_answer = en_word
        elif progress == 0.4:
            text += f"\U0001F1EC\U0001F1E7 *{en_word.upper()}*\nВыберите правильный ответ"
            buttons = [ru_word]
            i = 0
            while i < 3 and len(buttons) < 4:
                word = choice(raw_word_list)[2]
                if word not in buttons:
                    buttons.append(word)
                    i += 1
            shuffle(buttons)
            correct_answer = ru_word
        elif progress == 0.6:
            text += f"\U0001F1F7\U0001F1FA *{ru_word.upper()}*\nНапишите это слово на английском"
            correct_answer = en_word
        elif progress == 0.8:
            text += f"\U0001F1EC\U0001F1E7 *{en_word.upper()}*\nВы запомнили это слово?"
            buttons = ['Я запомнил', 'Нет, я не помню']
            correct_answer = 'Я запомнил'

    buttons.append(coach_fields[-1])
    markup = create_markup(buttons, 2)
    return text, markup, correct_answer, word_id, progress, sound
