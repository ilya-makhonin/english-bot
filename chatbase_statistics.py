import json
import requests
import time


class Statistics(object):

    def __init__(self, api_key):
        self.api_key = api_key
        self.platform = "Telegram"

    def send_statistics(self, user_id, intent="", message="", not_handled=False,
                        message_type="user", version="", session_id=""):
        if message_type == "agent" and not_handled is True:
            not_handled = False
        url = "https://chatbase.com/api/message"
        data = {
            "api_key": self.api_key,
            "type": message_type,
            "user_id": user_id,
            "time_stamp": int(round(time.time() * 1e3)),
            "platform": self.platform,
            "message": message,
            "intent": intent,
            "not_handled": not_handled,
            "version": version,
            "session_id": session_id
        }
        requests.post(url,
                      data=json.dumps(data),
                      headers={'Content-type': 'application/json', 'Accept': 'text/plain'})
