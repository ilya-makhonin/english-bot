import psycopg2
from states.errors import UserNotFoundException
from states.errors import AnotherTestException
from telebot import types
from utils import *
import sql
import constants


class States:
    """
    Test states arch

    {message.chat.id: {
                'test_id': id of the test in 'test_data' dict
                'test_name': name of the test
                'question_index': user's progress in test
                'progress': (list) all answers of user
            }}
    """
    tests_data = {}

    def __init__(self, url, port, dbname, login, password):
        self.db = Database(url, port, dbname, login, password)

    def handle(self, message, state):
        """
        Func for bot message_handler validation, use it like
            (func=lambda x: state.handle(x, %state_name%))
        :param message: <telebot.Message>
        :param state: <str> user's state for checking
        :return: True if it is user's state, False else
        """
        db_state = self.db.get_state(message.from_user.id)

        try:
            if state not in db_state:
                return False
            return True
        except TypeError:
            return False

    def change_state(self, message, state):
        """
        Just change state in database
        :param message: <telebot.Message>
        :param state: <str>
        :return: <None>
        """
        self.db.set_state(message.from_user.id, state)

    def get_current_test(self, test_id) -> dict:
        """
        :param test_id: <int> id of test in database
        :return: <dict> cached test data for specified user
        """
        data = self.tests_data.get(test_id)
        if data is None:
            data = self.load_test(test_id)
        return data

    def start_test(self, message, bot, test_id, table="tests"):
        """
        Init a test. Made for a simple test start
        :param message: <telebot.Message>
        :param bot: <telebot.Telebot>
        :param table: <str> table name
        :param test_id: <int> id of test in database
        :return: <None>
        """
        self.change_state(message, f'test_{test_id}_0_0')
        data = self.load_test(test_id, table)
        bot.send_message(message.from_user.id, test_started.format(data['name'], len(data['questions'])),
                         parse_mode="Markdown", reply_markup=types.ReplyKeyboardRemove())
        self.handle_test(message, bot, [test_id, 0, 0])

    def load_test(self, test_id, table="tests"):
        """
        Load test in cache
        :param table: <str> table name in database
        :param test_id: <int> id of test in database
        :return: <None>
        """
        if self.tests_data.get(test_id) is None:
            con = sql.get_connection(True)
            cursor = con.cursor()
            cursor.execute(f"SELECT * FROM {table} WHERE id=%s", (test_id,))
            data = cursor.fetchone()
            self.tests_data.update({test_id: data})
        return self.tests_data.get(test_id)

    def handle_test(self, message, bot, user_data):
        """

        :param message: <telebot.Message> user's message
        :param bot: <telebot.Telebot> for sending message to user
        :param user_data: <list> all data for current test (test id, question index, user progress)
        :return: <list> all user's answers in a row
        """
        user_id = message.from_user.id
        test_id, question_index, progress = [int(item) for item in user_data]

        data = self.get_current_test(test_id)

        # does it the first message check
        if int(question_index) != 0:
            if not message.data.isdigit():
                return
            correct_answer = data['answers'][question_index - 1]
            if data['questions'][question_index - 1][int(message.data)] == correct_answer:
                bot.edit_message_text(message.message.text + f"\n{correct_message}\n<b>{correct_answer}</b>", user_id,
                                      message.message.message_id, parse_mode="HTML")
                progress += 1
            else:
                bot.edit_message_text(message.message.text + f"\n\U0000274C Неправильно!\nВерный ответ - <b>{correct_answer}</b>",
                                      user_id, message.message.message_id, parse_mode="HTML")

        try:
            question_query = data['questions'][question_index]
        except IndexError:
            question_query = None

        if question_query is not None:
            question = question_query[0]

            markup = types.InlineKeyboardMarkup(row_width=1)
            # markup contain different answers (callback data is identifier of answer)
            for i in range(1, len(question_query)):
                markup.add(types.InlineKeyboardButton(question_query[i], callback_data=str(i)))
            markup.add(types.InlineKeyboardButton(constants.test_exit_message, callback_data=test_exit_message))

            bot.send_message(user_id, question, reply_markup=markup)
            question_index += 1
            self.change_state(message, f'test_{test_id}_{question_index}_{progress}')

            return None
        else:
            result = f"*{progress}* из *{len(data['questions'])}*"
            result += update_test_progress(user_id, test_id, result, progress)
            return result, progress, test_id

    @staticmethod
    def __get_question(question_id, data):
        """
        :param question_id: <int> question id
        :param data: <str[][]> all test_data
        #TODO: to return <str,markup> add all answers in markup
        :return: <str> For now returns all (question and all answers in one str var)
        """
        try:
            ans = f"{question_id}.{data[question_id][0]}\n"
            for i in range(1, len(data[question_id])):
                ans += f"{i}).{data[question_id][i]} \n"
            return ans
        except IndexError:
            return None


class Database:

    def __init__(self, url, port, dbname, login, password):
        self.connection = psycopg2.connect(dbname=dbname, host=url, port=port, user=login, password=password)

    def __del__(self):
        try:
            self.connection.close()
        except AttributeError:
            # TODO logger message
            pass

    def get_state(self, chat_id):
        """
        :param chat_id: <int/string> telegram user chat_id
        :return: <str> user's current state
        """
        with self.connection.cursor() as cursor:
            cursor.execute("SELECT (state) FROM users WHERE user_id=%s", (chat_id,))
            try:
                return cursor.fetchone()[0]
            except TypeError:
                print(UserNotFoundException(f"user {chat_id} was not found in database"))
            except Exception as e:
                raise e

    def set_state(self, chat_id, state):
        with self.connection.cursor() as cursor:
            try:
                cursor.execute("UPDATE users SET state=%s WHERE user_id=%s", (state, chat_id))
                self.connection.commit()
            except Exception as e:
                raise e
